#include "car.h"
#include "track.h"
#include "track_piece.h"

car_position::car_position()
{
	pieceIndex = 0;
	inPieceDistance = 0;
	startLaneIndex = 0;
	endLaneIndex = 0;
	lap = 0;
	angle = 0;
}

car_position::car_position(const jsoncons::json &data)
{
	angle = data.get("angle", 0).as<double>();

	const auto &piecePosition = data["piecePosition"];
	lap = piecePosition.get("lap", 0).as<int>();
	pieceIndex = piecePosition.get("pieceIndex", 0).as<int>();
	inPieceDistance = piecePosition.get("inPieceDistance", 0).as<double>();

	const auto &lane = piecePosition["lane"];
	startLaneIndex = lane.get("startLaneIndex", 0).as<int>();
	endLaneIndex = lane.get("endLaneIndex", 0).as<int>();

	std::cout << "car_position(); angle = " << angle << "; pieceIndex = " << pieceIndex << "; inPieceDistance = " << inPieceDistance;
	std::cout << "; startLaneIndex = " << startLaneIndex << "; endLaneIndex = " << endLaneIndex << "; lap = " << lap << "\n";
}

car::car()
{
	_color = "";
	_name = "";
	_length = 0.0;
	_width = 0.0;
	_guideFlagPosition = 0.0;
	_track = nullptr;
	_velocity = 0;
}

car::car(const jsoncons::json& data, track *t)
{
	_track = t;
	const auto &id = data["id"];
	_name = id.get("name", "").as<std::string>();
	_color = id.get("color", "").as<std::string>();

	const auto &dimensions = data["dimensions"];
	_length = dimensions.get("length", 0).as<double>();
	_width = dimensions.get("width", 0).as<double>();
	_guideFlagPosition = dimensions.get("guideFlagPosition", 0).as<double>();

	std::cout << "car(): " << "name : " << _name << "; color = " << _color << "; length = " << _length << "; width = " << _width << "; guideFlagPosition = " << _guideFlagPosition << "\n";
}

void car::update(const jsoncons::json& data)
{
	updatePosition(data);
	updateCurrentVelocity();
}

void car::updatePosition(const jsoncons::json& data)
{
	_prevPosition = _currPosition;
	_currPosition = car_position(data);
}

void car::updateCurrentVelocity()
{
	if (!_track)
	{
		std::cout << "Error! No track pointer!\n";
		return;
	}

	// На том же участке трека
	if (_prevPosition.pieceIndex == _currPosition.pieceIndex)
	{
		// Текущий участок трека
		track_piece &piece = _track->pieceAtIndex(_prevPosition.pieceIndex);
		// Поворот
		if (piece.isBend())
		{

		}
		else
		{
			_velocity = _currPosition.inPieceDistance - _prevPosition.inPieceDistance;
		}
	}
	else
	{
	}
}