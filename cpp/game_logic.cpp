#include "game_logic.h"
#include "protocol.h"

using namespace hwo_protocol;

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error },
      { "yourCar", &game_logic::on_your_car },
      { "gameInit", &game_logic::on_game_init },
      { "lapFinished", &game_logic::on_lap_finished },
    }
{
  init();
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);

  if (msg_type == "carPositions")
    _tick = msg.get("gameTick", -1).as<int>();

  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
  const auto &race = data["race"];

  const auto &track = race["track"];
  _track.init_with_json(track);

  const auto &cars = race["cars"];
  for (size_t i = 0; i < cars.size(); ++i)
  {
    car c = car(cars[i], &_track);
    if (c.color() == _carColor)
      _myCar = c;
    else
      _cars.push_back(c);
  }

  const auto &race_session = race["raceSession"];
  _raceSession.init_with_json(race_session);

  std::cout << "my car color is " << _myCar.color() << "\n";
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
  updateCarPositions(data);

  std::cout << "tick = " << _tick << "; velocity = " << _myCar.velocity() << "\n";
  double throttle = 0.5f;
  if (_tick < 50)
    throttle = 1.0f;
  else
    throttle = 0.0f;
  return { make_throttle(throttle) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::cout << "Someone crashed" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_lap_finished(const jsoncons::json& data)
{
  std::cout << "Lap finished" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
  _carColor = data.get("color", "").as<std::string>();
  std::cout << "You car: name = " << data["name"] << ", color = " << _carColor << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}

void game_logic::init()
{
  _carColor = "";
  _tick = 0;
}

void game_logic::updateCarPositions(const jsoncons::json& data)
{
  for (size_t i = 0; i < data.size(); ++i)
  {
    const auto &id = data[i]["id"];
    const std::string name = id.get("name", "").as<std::string>();
    const std::string color = id.get("color", "").as<std::string>();

    car &c = getCarByColor(color);
    if (c.color() == color)
    {
      c.update(data[i]);
    }
  }
}

car & game_logic::getCarByColor(const std::string &color)
{
  for (size_t i = 0; i < _cars.size(); ++i)
  {
    if (color == _cars[i].color())
      return _cars[i];
  }

  if (_myCar.color() == color)
    return _myCar;

  std::cout << "Error! Car not found! color = " << color << "\n";
  static car dummy_car;
  return dummy_car;
}