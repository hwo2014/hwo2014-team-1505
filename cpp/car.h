#ifndef _CAR_H_
#define _CAR_H_

#include <iostream>
#include <jsoncons/json.hpp>

class track;
struct car_position
{
	double angle;
	double inPieceDistance;
	int pieceIndex;
	int startLaneIndex;
	int endLaneIndex;
	int lap;

	car_position();
	car_position(const jsoncons::json &data);
};

class car
{
public:
	car();
	car(const jsoncons::json& data, track *t);

	void update(const jsoncons::json& data);

	std::string color() const { return _color; }
	std::string name() const { return _name; }
	double length() const { return _length; }
	double width() const { return _width; }
	double guideFlagPosition() const { return _guideFlagPosition; }
	double velocity() const { return _velocity; }

private:
	void updatePosition(const jsoncons::json& data);
	void updateCurrentVelocity();

private:
	// Указатель на трек (для доступа к кускам трека)
	track *_track;

	// Параметры машины
	std::string _color;
	std::string _name;
	double _length;
	double _width;
	double _guideFlagPosition;

	// Позиция (текущая и предыдущая)
	car_position _currPosition;
	car_position _prevPosition;

	// Текущая скорость
	double _velocity;
};
#endif