#include <iostream>
#include <functional>
#include "track.h"

void track::init_with_json(const jsoncons::json& data)
{
	_id = data["id"].as<std::string>();
	_name = data["name"].as<std::string>();

	const auto &pieces = data["pieces"];
	for (size_t i = 0; i < pieces.size(); ++i)
		_pieces.push_back(track_piece(pieces[i]));

	const auto &lanes = data["lanes"];
	for (size_t i = 0; i < lanes.size(); ++i)
		_lanes.push_back(track_lane(lanes[i]));

	std::cout << "track::init_with_json(), id = " << _id << ", " << "name = " << _name << "\n";
}

track_piece & track::pieceAtIndex(int index)
{
	return _pieces[index];
}