#include "race_session.h"

race_session::race_session()
{
	_laps = 0;
	_maxLapTimeMs = 0;
	_quickRace = false;
}

void race_session::init_with_json(const jsoncons::json& data)
{
	_laps = data.get("laps", 0).as<int>();
	_maxLapTimeMs = data.get("maxLapTimeMs", 0).as<int>();
	_quickRace = data.get("quickRace", false).as<bool>();

	std::cout << "race_session(): laps = " << _laps << "; maxLapTimeMs = " << _maxLapTimeMs << "; quickRace = " << _quickRace << "\n";
}
