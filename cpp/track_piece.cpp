#include "track_piece.h"
#include <iostream>

track_piece::track_piece()
{
	_length = 0;
	_radius = 0;
	_angle = 0;
	_has_switch = false;
}

track_piece::track_piece(const jsoncons::json& data)
{
	_length = data.get("length", 0).as<double>();
	_radius = data.get("radius", 0).as<double>();
	_angle = data.get("angle", 0).as<double>();
	_has_switch = data.get("switch", false).as<bool>();

	std::cout << "track_piece(): " << "length : " << _length << ", radius: " << _radius << ", angle: " << _angle << "; switch: " << _has_switch << "\n";
}