#ifndef _GAME_TRACK_H_
#define _GAME_TRACK_H_

#include <vector>
#include <iostream>
#include <jsoncons/json.hpp>
#include "track_piece.h"
#include "track_lane.h"

// Трек
class track
{
public:
	track() {}

	void init_with_json(const jsoncons::json& data);

	track_piece & pieceAtIndex(int index);
private:
	std::string _id;
	std::string _name;

	std::vector<track_piece> _pieces;
	std::vector<track_lane> _lanes;
};
#endif