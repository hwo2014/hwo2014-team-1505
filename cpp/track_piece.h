#ifndef _TRACK_PIECE_H_
#define _TRACK_PIECE_H_

#include <iostream>
#include <jsoncons/json.hpp>

class track_piece
{
public:
	track_piece();
	track_piece(const jsoncons::json& data);

	double length() const { return _length; }
	double radius() const { return _radius; }
	double angle() const { return _angle; }
	bool has_switch() const { return _has_switch; }

	bool isBend() const { return _radius != 0.0f; }
private:
	// Длина
	double _length;

	// Радиус
	double _radius;

	// Угол сегмента
	double _angle;

	// Есть переключатель на участке.
	bool _has_switch;
};
#endif