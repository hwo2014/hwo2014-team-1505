#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>
#include "track.h"
#include "car.h"
#include "race_session.h"

class game_logic
{
public:
  typedef std::vector<jsoncons::json> msg_vector;

  game_logic();
  msg_vector react(const jsoncons::json& msg);

private:
  void init();

  typedef std::function<msg_vector(game_logic*, const jsoncons::json&)> action_fun;
  const std::map<std::string, action_fun> action_map;

  msg_vector on_join(const jsoncons::json& data);
  msg_vector on_game_start(const jsoncons::json& data);
  msg_vector on_car_positions(const jsoncons::json& data);
  msg_vector on_crash(const jsoncons::json& data);
  msg_vector on_game_end(const jsoncons::json& data);
  msg_vector on_error(const jsoncons::json& data);
  msg_vector on_your_car(const jsoncons::json& data);
  msg_vector on_game_init(const jsoncons::json& data);
  msg_vector on_lap_finished(const jsoncons::json& data);

  void updateCarPositions(const jsoncons::json& data);

  car & getCarByColor(const std::string &color);

private:
  // Цвет машины
  std::string _carColor;

  // Трек
  track _track;

  // Машины
  std::vector<car> _cars;

  // Моя машина
  car _myCar;

  // Инфа о заезде
  race_session _raceSession;

  int _tick;
};

#endif
