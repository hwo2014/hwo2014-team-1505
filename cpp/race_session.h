#ifndef _RACE_SESSION_H_
#define _RACE_SESSION_H_

#include <iostream>
#include <jsoncons/json.hpp>

class race_session
{
public:
	race_session();
	void init_with_json(const jsoncons::json& data);

	int laps() const { return _laps; }
	int maxLapTimeMs() const { return _maxLapTimeMs; }
	bool quickRace() const { return _quickRace; }
private:
	int _laps;
	int _maxLapTimeMs;
	bool _quickRace;
};
#endif