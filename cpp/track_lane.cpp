#include "track_lane.h"
#include <iostream>

track_lane::track_lane()
{
	_distance_from_center = 0;
	_index = 0;
}

track_lane::track_lane(const jsoncons::json& data)
{
	_distance_from_center = data.get("distanceFromCenter", 0).as<double>();
	_index = data.get("index", 0).as<int>();

	std::cout << "track_lane(): distanceFromCenter: " << _distance_from_center << ", index: " << _index << "\n";
}
