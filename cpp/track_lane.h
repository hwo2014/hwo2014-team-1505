#ifndef _TRACK_LANE_H_
#define _TRACK_LANE_H_

#include <iostream>
#include <jsoncons/json.hpp>

class track_lane
{
public:
	track_lane();
	track_lane(const jsoncons::json& data);

	double distance_from_center() const { return _distance_from_center; }
	int index() const { return _index; }
private:
	double _distance_from_center;
	int _index;
};
#endif